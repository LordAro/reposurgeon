= DVCS migration HOWTO =
:description: How to migrate old repositories to distributed version control
:keywords: CVS, Subversion, svn, git, hg, RCS, darcs, bzr, bk, conversion, version control
Eric S. Raymond <esr@thyrsus.com>

== Content doesn't live here any more ==

The material that used to live here no longer exists as a serparate
dpcument.  It has been incorpotrated into the long-form documrnation
for reposurgeon; look for "Repository Editing and Conversion with
Reposurgeon", specifically the major section titled
http://www.catb.org/~esr/reposurgeon/repository-editing.html#conversion[A
Guide to Repository Conversion].

// Local Variables:
// compile-command: "make dvcs-migration-guide.html"
