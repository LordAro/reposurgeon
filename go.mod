// -*- mode: Go;-*-
module gitlab.com/esr/reposurgeon

require (
	github.com/anmitsu/go-shlex v0.0.0-20161002113705-648efa622239
	github.com/chzyer/logex v1.1.10 // indirect
	github.com/chzyer/test v0.0.0-20180213035817-a1ea475d72b1 // indirect
	github.com/emirpasic/gods v1.12.0
	github.com/flynn/go-shlex v0.0.0-20150515145356-3f9db97f8568 // indirect
	github.com/ianbruene/go-difflib v1.1.2
	github.com/kballard/go-shellquote v0.0.0-20180428030007-95032a82bc51
	github.com/orcaman/concurrent-map v0.0.0-20190826125027-8c72a8bb44f6
	github.com/termie/go-shutil v0.0.0-20140729215957-bcacb06fecae
	gitlab.com/esr/fqme v0.1.0
	gitlab.com/ianbruene/kommandant v0.6.0
	golang.org/x/crypto v0.0.0-20191206172530-e9b2fee46413
	golang.org/x/sys v0.0.0-20191210023423-ac6580df4449 // indirect
	golang.org/x/text v0.3.2
)

go 1.13
